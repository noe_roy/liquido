import itertools
import numpy as np
import matplotlib.pyplot as plt
import time
import random

def extract_baseline(y):
    nrow, ncol  = y.shape #Gets number of rows (hits) and columns (points/pulses) in the selection of hits
    max_arg = np.argmax(y,1) # Gets the array of all the max amplitude position
    good_max_arg = max_arg[:,None] * np.ones((nrow,ncol)) 
    count = np.arange(1,ncol+1)[None] * np.ones((nrow,ncol)) 
    y_baseline = np.copy(y)
    y_baseline[good_max_arg -3 < count] = np.nan # Creates the pulses array (y) but with only the amplitude values 3 bins before the max
    baselines = np.nanmean(y_baseline,1) # Measure the average of the baseline
    baselines_std = np.nanstd(y_baseline,1)
    return (baselines,baselines_std)

def peak_numbers(y):
    nrow, ncol  = y.shape #Gets number of rows (hits) and columns (points/pulses) in the selection of hits
    f_col = np.array([False]*nrow).reshape(nrow,1)
    ff_col = np.array([False,False]*nrow).reshape(nrow,2)
    val_sup = np.hstack([f_col,(y[:,1:] > y[:,:-1])]) # Ai > Ai-1
    val_inf =  np.hstack([(y[:,:-1] > y[:,1:]),f_col]) # Ai > Ai+1
    prev_sup = np.hstack([ff_col,(y[:,1:-1]>=y[:,:-2])]) # Ai-1 >= Ai-2
    prev_inf = np.hstack([(y[:,1:-1] >= y[:,2:]),ff_col]) # Ai+1 >= Ai+2
    tot_peak = (val_sup & val_inf & prev_sup & prev_inf)
    return tot_peak.sum(1)

def draw_pulse(ax,fig,x,y,n_pulse):
    ax.clear()
    col = np.random.rand(3,)
    ax.fill_between(x,y[n_pulse], alpha=0.2)
    ax.plot(x,y[n_pulse],label=f'pulse #{n_pulse}')
    ax.legend()
    fig.canvas.draw()