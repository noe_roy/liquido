import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import time
import random
import pandas as pd
import sys
import scipy
from scipy import stats  
from scipy.signal import find_peaks
from matplotlib.backends.backend_pdf import PdfPages

#Gaussian function
def _1gaussian(x, amp1,cen1,sigma1):
    return amp1*(1/(sigma1*(np.sqrt(2*np.pi))))*(np.exp((-1.0/2.0)*(((x-cen1)/sigma1)**2)))

#Computes the Chi2 for a given prediction + distribution
def compute_chi2(func,x,y,sigmas):
    chi2=0
    pred = func(x)
    chi2 = ((pred - y)**2/(sigmas**2)).sum()
    return chi2

#Computes the Chi2 for a given prediction + distribution
def compute_R2(func,x,y):
    mean=y.mean()
    sstot=((y - mean)**2).sum()
    ssreg= ((func(x) - mean)**2).sum()
    ssres = ((func(x) - y)**2).sum()
    R2 = 1 - (ssres/sstot)
    return R2

#Computes the PAs value with a fixed set of cut (could be updated to take a set of cut as entry)
def get_pas(j,df,plot=False):
    xx = np.array(df["Charge"][(df["Channel"] == j) & (df["Baseline_std"] < 0.0025) & (df["Q_A"] < 0.002) & (df["n_max"] < 4) ])
    hist = plt.hist(xx, bins=100, label='standard cuts',range=[-0.5e-4,2e-4])
    hist_array = np.array(hist)
    p, _= find_peaks(hist_array[0],prominence=20,distance=10)
    gaussians = []
    npa = np.arange(1,hist_array[1][p][hist_array[1][p] > 2e-5].size+1)
    chpa = np.zeros(hist_array[1][p][hist_array[1][p] > 2e-5].size)
    errpa = np.zeros(hist_array[1][p][hist_array[1][p] > 2e-5].size)
    i=0
    pivot = 1e-5
    for peak, max_p in zip(hist_array[1][p],hist_array[0][p]):
        if peak < 2e-5 :
            continue
        bd = ((-np.inf, peak-1e-5, 3e-6),(np.inf,  peak+1e-5, 4e-6))
        popt_gauss, pcov_gauss = scipy.optimize.curve_fit(_1gaussian, hist_array[1][np.argwhere((hist_array[1]<peak+pivot) & (hist_array[1]>peak-pivot) ).T][0], hist_array[0][np.argwhere((hist_array[1]<peak+pivot) & (hist_array[1]>peak-pivot) ).T][0], p0=[1e-3, peak, 3.5e-6], bounds=bd)
        gaussians.append(_1gaussian(hist_array[1][1:], popt_gauss[0], popt_gauss[1]+0.375e-5, popt_gauss[2]))
        chpa[i] = popt_gauss[1]+0.375e-5
        errpa[i] = popt_gauss[2]
        i+=1
    if plot:
        return (chpa,npa,errpa,gaussians,hist_array)
    else :
        return (chpa,npa,errpa)



def draw_fitted_histo(ch,df,plot=True):
    pivot = 1e-5
    #ax.clear()
    fig,ax = plt.subplots(1,1)
    chpa,npa,errpa,gaussians,hist_array = get_pas(ch,df,True)
    plt.title(f'channel {ch}')
    plt.ticklabel_format(axis='x',style='sci', scilimits=(-2,2))
    plt.xlabel('Charge [mV.ns]')
    for gaus in gaussians:
        plt.plot(hist_array[1][:-1],gaus,"--",color="red")
    plt.yscale("symlog")
    plt.legend()
    plt.draw()
    if plot :
        plt.show() 

#Draw a given number of charge histo fitted by gaussians and stored after in the pdf "namefile". Charge must come from df dataframe
def draw_all_fitted_histo(start,stop,namefile,df):
    with PdfPages(namefile) as pp :
        for ch in range (start,stop):
            draw_fitted_histo(ch,df,False)
            pp.savefig()


            
#Compute the Gain of a given df dataframe 
def get_gains(ch,df,plot=False):
    chpa,npa, errpa = get_pas(ch,df)
    gain, pedest, r_value, p_value, std_err = stats.linregress(npa, chpa)
    f = lambda x : gain * x + pedest
    r2 = compute_R2(f,npa,chpa)
    chi2 = compute_chi2(f,npa,chpa,errpa)
    if plot:
        return (gain,pedest,chi2,r2,chpa,npa,errpa)
    else:
        return (gain,pedest,chi2,r2)

#Plot gain fit for a given channel
def plot_gain(ch,df,plot=True):
    fig,ax = plt.subplots(1,1)
    #plt.plot(npa2,chpa2,'s',linewidth=0,)
    slope, intercept,chi2,r2,chpa,npa,errpa = get_gains(ch,df,True)
    ax.clear()
    plt.errorbar(npa,chpa,fmt = 'o', yerr = errpa)
    plt.plot(npa, intercept + slope*npa,'--', label='fitted line')
    plt.text(1,1e-4,f'y = {slope:.3} x + {intercept:.3}',color='C1')
    plt.title(f'Channel {ch}')
    ndf = npa.size - 2
    plt.text(1,0.8e-4,fr'$\frac{{\chi^2}}{{ndf}} ={chi2/ndf:.3} $',color='C0')
    plt.text(1,0.7e-4,f'R² ={r2:.4}',color='C0')
    if plot:
        plt.show()

#Plot gain fit for all channels and store it into the pdf "namefile"
def plot_all_gains(start,stop,namefile,df):
    with PdfPages(namefile) as pp :
        for ch in range (start,stop):
            plot_gain(ch,df,False)
            pp.savefig()

